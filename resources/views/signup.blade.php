<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>

    <!--============== LINK CSS ===================-->
    <link rel="stylesheet" href="/css/login.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  <body>

<div class="container">
        <div class="row align-items-center justify-content-center vh-100">
            <div class="col-lg-9">

                <div class="shadow">
                    <div class="row mb-5 mt-5">
                        <div class="col-lg-5">
                            <div class="bg-login h-100">
                                <p class="mb-4 text-muted">Capturing your moments with us.
                                     <br> Kita siap ikutin kemana aja.</p>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="p-5 ps-4 text-dark">
                                <h5 class="mb-1 fw-bold">Selamat Datang!</h5>
                                <h5 class="mb-1 fw-bold">Sign Up Dulu yaa</h5>
                                <br>
                                
                                    <div class="row mb-3">
                                        <div class="col">

                                        <div class="mb-3">
                                                <label for="name" class="form-label">Name</label>
                                                <input type="name" class="form-control" id="name" name="name_user" required autofocus>
                                            </div>

                                            <div class="mb-3">
                                                <label for="email" class="form-label">Email Address</label>
                                                <input type="email" class="form-control" id="email" name="email_user" required autofocus>
                                            </div>

                                            <div class="row mb-3">
                                                <div class="col">
                                                    <label for="pass" class="form-label">Password</label>
                                                    <input type="password" class="form-control" id="pass" name="password_user" required>
                                                    
                                                </div>
                                                <div class="mb-3">
                                                    <div class="col d-grid d-inline-block mb-3 mt-4">
                                                        <button type="submit"
                                                            class="btn btn-primary py-2">Sign Up</button>
                                                    </div>
                                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html>